/*! \file ExFce_main.c

    \brief UI for a two segment thermostat with plotting

	Program runs on an Explorer 16 board with a Graphics PICtail Plus.
	It expects a PIC24FJ128GA010 and a Microtips PICtail.

	The entire UI is not yet implemented. Up/Down only works on
	the home screen, and hold/vacation hold is not implemented.
	In addition, the RTC is not implemented.

        Current memory use: Flash 59,367, data 6,490, heap and
	stack 1,702.

The following diagrams show the transitions between
the various screens in the application
\dotfile HomeTransition.dot "Transitions to/from home screen"
\dotfile TempTransition.dot "Transitions to/from temperature screen"
\dotfile SkedTransition.dot "Transitions to/from schedule screen"
\dotfile HistTransition.dot "Transitions to/from history screen"
*/

#include "ExFce.h"
#include <string.h>

//! Background window
#define ID_WINDOW1     	10

//! Set Temperature button
#define ID_BUTTEMP      51
//! Set schedule button
#define ID_BUTSKED      52
//! View history button
#define ID_BUTHIST      53
//! Set time button
#define ID_BUTTIME      54
//! Set daytime temperature button
#define ID_BUTNITE      55
//! Set nighttime temperature button
#define ID_BUTDAY       56
//! Set hold button
#define ID_BUTHOLD      57
//! Return to home screen button(s)
#define ID_BUTOK        59

//! Setpoint up (main screen) button
#define ID_BUTUP1		71
//! Setpoint down (main screen) button
#define ID_BUTDN1		72
//! Setpoint up (temperature screen) button
#define ID_BUTUP2		73
//! Setpoint down (temperature screen) button
#define ID_BUTDN2		74
//! Daytime hours up button
#define ID_BUTUP3		75
//! Daytime hours down button
#define ID_BUTDN3		76
//! Daytime minutes up button
#define ID_BUTUP4		77
//! Daytime minutes down button
#define ID_BUTDN4		78
//! Nighttime hours up button
#define ID_BUTUP5		79
//! Nighttime hours down button
#define ID_BUTDN5		80
//! Nighttime minutes up button
#define ID_BUTUP6		81
//! Nighttime minutes down button
#define ID_BUTDN6		82

//! Static Text 1
#define ID_TEXT1		41
//! Static Text 2
#define ID_TEXT2		42
//! Static Text 3
#define ID_TEXT3		43
//! Static Text 4
#define ID_TEXT4		44
//! Static Text 5
#define ID_TEXT5		45
//! Static Text 6
#define ID_TEXT6		46
//! Static Text 7
#define ID_TEXT7		47
//! Static Text 8
#define ID_TEXT8		48

//! Setpoint text
#define ID_TEXTSP		49

//! Color scheme for buttons
GOL_SCHEME*    alt3Scheme;		 // alternative style scheme
//! Background window bright yellow text color scheme
GOL_SCHEME*    alt4Scheme;		 // alternative style scheme
//! Background window color scheme
GOL_SCHEME*    wndScheme;		 // alternative style scheme
//! Background window (de-emphasized text) color scheme
GOL_SCHEME*    wnd2Scheme;		 // alternative style scheme

//! Page number to display, 0=keep old page
int nNewPage;
//! Currently active setpoint
int nSetpoint;
//! Daytime setpoint
int nSetDay;
//! Night setpoint
int nSetNight;
//! Temporary hold
int nHold;
//! Permanent hold
int nVacHold;
//! Displayed setpoint as string
static char szSetpoint[16];

void makeSchemes( void );
void makePage( int );

//! main - Mainline for PIC24 Thermostat
/*!
\b Pseudocode:
\code
Initialize EEPROM
Inititlize graphics library
Initialize touch Screen
IF PB3 is pressed
    Calibrate touch screen
IF touch screen not calibrated ever
    Calibrate touch screen
Initialize application variables
WHILE 1
    if GOLDraw()
        Get touch screen message
        Process message
        IF nNewPage != 0
            makePage()
\endcode
*/
int main(void){
GOL_MSG msg;        			// GOL message structure to interact with GOL


    EEPROMInit();   			// Initialize EEPROM
    //TickInit();     			// Start tick counter    
    GOLInit();      			// Initialize graphics library and crete default style scheme for GOL
    //BeepInit();     			// Initialize beeper
    TouchInit();    			// Initialize touch screen
    //RTCCInit(); 				// Setup the RTCC
    //RTCCProcessEvents();

    // If S3 button on Explorer 16 board is pressed calibrate touch screen
    if(PORTDbits.RD6 == 0){
        TouchCalibration();
        TouchStoreCalibration();
    }

    // If it's a new board (EEPROM_VERSION byte is not programed) calibrate touch screen
    if(GRAPHICS_LIBRARY_VERSION != EEPROMReadWord(EEPROM_VERSION)){
        TouchCalibration();
        TouchStoreCalibration();
        EEPROMWriteWord(GRAPHICS_LIBRARY_VERSION,EEPROM_VERSION);
    }

    // Load touch screen calibration parameters from EEPROM
    TouchLoadCalibration();

	nSetpoint = 72;
	nSetDay = nSetpoint;
	nSetNight = 65;
	nHold = 1;
	nVacHold = 0;
	makeSchemes();
	nNewPage = 1;
	makePage(1);

   while(1){
        if(GOLDraw()){             // Draw GOL objects
            // Drawing is done here, process messages
            TouchGetMsg(&msg);     // Get message from touch screen
            GOLMsg(&msg);          // Process message
			if ( nNewPage )
				makePage(nNewPage);
        }
    } 

}

// These routines, stolen from K&R, should be in the library
// but I can't find them.

//! reverse - reverse a character string
void reverse( char *s )
{
  unsigned char c,i,j;

  for ( i = 0, j = strlen(s)-1; i < j; i++, j-- )
    {
      c = s[i];
      s[i] = s[j];
      s[j] = c;
    }
}

//! itoa - generate ASCII representation of an integer
void itoa( int n, char *s )
{
  int i, sign;

  if ((sign = n) < 0 )
    n = -n;
  i = 0;
  do {
    s[i++] = n % 10 + '0';
  } while ((n /= 10) > 0);
  if (sign < 0 )
    s[i++] = '-';
  s[i] = '\0';
  reverse( s );
}

//! formSetpoint - convert nSetpoint into properly formatted szSetpoint string
/*!
\b Pseudocode:
\code
Convert setpoint to string
Append " F" to string
\endcode
*/
void formSetpoint()
{
	itoa(nSetpoint,szSetpoint);
	strcat(szSetpoint," F");
}

//! GOLMsgCallback - Graphics library message handler
/*!
\b Pseudocode:
\code
Get object ID
CASE Object ID
    OK Button
        Queue up home screen
    Temp Button
        Queue up temperature screen
    Sked Button
        Queue up schedule screen
    Hist button
        Queue up history screen
    Day button
        Set Day button pressed
        Set Night button unpressed
    Night button
        Set Night button pressed
        Set Day button unpressed
    Up button on main screen
        Increment setpoint
        Form new setpoint string
        Force redraw of setpoint
    Dn button on main screen
        Decrement setpoint
        Form new setpoint string
        Force redraw of setpoint
ENDCASE
return 1
\endcode
*/
WORD GOLMsgCallback(WORD objMsg, OBJ_HEADER* pObj, GOL_MSG* pMsg)
{
	int nID;
	//int nState;
	OBJ_HEADER *pOtherObj;

	nID = GetObjID(pObj);
	switch ( nID )
	{
		case ID_BUTOK:		// OK always returns to home screen
			nNewPage = 1;
			break;
		case ID_BUTTEMP:	// Select temperature setting
			nNewPage = 2;
			break;
		case ID_BUTSKED:	// Select schedule setting
			nNewPage = 3;
			break;
		case ID_BUTHIST:	// Select history setting
			nNewPage = 4;
			break;
		case ID_BUTDAY:		// Make day/night buttons act like radio buttons
			if ( objMsg == BTN_MSG_PRESSED )
			{
				pOtherObj = GOLFindObject( ID_BUTNITE );
				SetState(pObj,BTN_PRESSED|BTN_DRAW); 
				ClrState(pOtherObj,BTN_PRESSED); 
				SetState(pOtherObj,BTN_DRAW); 
			}
			break;
		case ID_BUTNITE:
			if ( objMsg == BTN_MSG_PRESSED )
			{
				pOtherObj = GOLFindObject( ID_BUTDAY );
				SetState(pObj,BTN_PRESSED|BTN_DRAW);
				ClrState(pOtherObj,BTN_PRESSED); 
				SetState(pOtherObj,BTN_DRAW); 
			}
			break;
		case ID_BUTUP1:
			if ( objMsg == BTN_MSG_PRESSED )
			{
				nSetpoint++;
				formSetpoint();
				pOtherObj = GOLFindObject( ID_TEXTSP );
				SetState(pOtherObj,BTN_DRAW); 
			}
			break;
		case ID_BUTDN1:
			if ( objMsg == BTN_MSG_PRESSED )
			{
				nSetpoint--;
				formSetpoint();
				pOtherObj = GOLFindObject( ID_TEXTSP );
				SetState(pOtherObj,BTN_DRAW); 
			}
			break;
		default:
			break;
	}

  return 1;
}

//! GOLDrawCallback - Special drawing function handler
/*! GOLDrawCallback is currently only a placeholder.  It simply
returns a 1.
*/
WORD GOLDrawCallback()
{
  return 1;
}

//! makeSchemes - Build the color schemes
/*! Creates all the color schemes used on the various screens.
 */
void makeSchemes( void )
{

  alt3Scheme = GOLCreateScheme(); 		// create alternative 1 style scheme
  alt4Scheme = GOLCreateScheme(); 		// create alternative 1 style scheme
  wndScheme = GOLCreateScheme(); 		// create alternative 1 style scheme
  wnd2Scheme = GOLCreateScheme(); 		// create alternative 1 style scheme

  /* for Microtips display */

  wndScheme->CommonBkColor = RGB565CONVERT(0x30, 0x40, 0x50);
  wndScheme->Color0 = wndScheme->CommonBkColor;
  wndScheme->Color1 = wndScheme->CommonBkColor;
  wndScheme->EmbossDkColor = RGB565CONVERT(0x10, 0x10, 0x20);
  wndScheme->EmbossLtColor = RGB565CONVERT(0x30, 0x30, 0x40);
  wndScheme->ColorDisabled = RGB565CONVERT(0x00, 0x00, 0xFf);
  wndScheme->TextColor1 = RGB565CONVERT(0x00, 0x00, 0xff);
  wndScheme->TextColor0 = RGB565CONVERT(0x7F, 0xff, 0x3f);
  wndScheme->TextColorDisabled = RGB565CONVERT(0xB8, 0xB9, 0xBC);
  memcpy(wnd2Scheme,wndScheme,sizeof(GOL_SCHEME));
  wnd2Scheme->TextColor0 = RGB565CONVERT(0x0, 0x7f, 0x0);

  alt3Scheme->Color0 = RGB565CONVERT( 0x20, 0x20, 0x20); 		
  alt3Scheme->Color1 = RGB565CONVERT( 0x10, 0x10, 0x10);
  alt3Scheme->TextColor0 = RGB565CONVERT( 0x0, 0xc0, 0x0);
  alt3Scheme->TextColor1 =  RGB565CONVERT( 0x0, 0xff, 0x0);
  alt3Scheme->EmbossDkColor = RGB565CONVERT(0x00, 0xc0, 0x0);
  alt3Scheme->EmbossLtColor = RGB565CONVERT(0x0, 0x30, 0x0);

  memcpy(alt4Scheme,wndScheme,sizeof(GOL_SCHEME));
  alt4Scheme->TextColor0 =  RGB565CONVERT( 0xff, 0xff, 0x0);
  alt4Scheme->EmbossDkColor = RGB565CONVERT(0x00, 0x00, 0x0);
  alt4Scheme->EmbossLtColor = RGB565CONVERT(0xcc, 0xcc, 0xcc);

}


// Data for graph (Fake)
//! List of outdoor temperatures for graph
static const int nOutdoor[] = {
    17,16,15,15,16,18,20,23,26,29,32,35,37,39,40,40,39,38,36,33,
    30,27,24,22,19,18,17,17,18,20,22,25,28,31,34,37,39,41,42,42,
    41,40,38,35,32,29,26,23,21,20,19,19,20,21,24,26,29,33,36,38,
    41,42,43,44,43,41,39,37,34,31,28,25,23,21,20,20,21,23,25,28,
    31,34,37,40,42,44,45,45,44,43,40,38,35,32,29,26,24,22,21,21,
    22,24,26,28,31,35,38,40,43,44,45,45,45,43,41,38,35,32,29,26,
    24,23,22,22,22,24,26,29,32,35,38,40,43,44,45,45,45,43,41,38,
    35,32,29,26,24,22,21,21,22,23,25,28,31,34,37,40,42,44,45,45,
    44,42,40,37,34,31,28,25};

//! List of indoor temperatures for graph
static const int nIndoor[] = {
    65,65,65,66,65,65,67,71,73,73,73,73,72,73,73,72,72,73,73,73,
    73,73,69,66,66,65,66,65,65,66,65,71,73,72,73,73,72,72,72,72,
    72,72,72,73,72,73,69,66,67,67,65,65,66,65,66,71,73,72,73,72,
    72,73,72,72,73,73,73,72,73,72,69,66,65,66,65,66,66,65,66,70,
    73,72,73,72,73,73,72,72,73,72,73,73,73,72,68,66,65,65,65,65,
    66,67,65,70,72,72,72,72,72,72,72,72,72,72,72,73,72,73,68,66,
    65,66,66,65,65,65,65,70,72,72,72,72,73,72,72,72,72,72,72,72,
    72,72,68,66,65,66,66,66,66,66,65,70,73,72,73,72,72,72,73,72,
    72,72,73,72,73,74,69,65};

//! Graph panel bottom
#define GR_PAN_BOT 190
//! Graph panel top
#define GR_PAN_TOP 5
//! Graph panel left
#define GR_PAN_LEF 5
//! Graph panel right
#define GR_PAN_RIG 225

//! makePage - Layout each of the various screens
/*!
\b Pseudocode:
\code
IF nNewPage == 0
    return
Free old list of objects
Set current color to black
Clear the device
CASE nPageNum
    1:
        Lay out buttons and text for home screen
    2:
        Lay out buttons and text for temperature setting screen
    3:
        Lay out buttons and text for schedule screen
    4:
        Draw graph of last 3 days temperatures
ENDCASE
nNewPage = 0
\endcode
*/
void makePage( int nPageNum )
{
OBJ_HEADER* obj;
int x,xp1,xp2,yp1,yp2;
char szWork[16];

    //makeSchemes();
	if ( !nNewPage )
		return;

	GOLFree();
	//SetColor( BLACK );
	SetColor( wndScheme->CommonBkColor );
	ClearDevice();

    switch( nPageNum )
	{
	case 1:		// - - - - - - H o m e   W i n d o w - - - - - - 
	    obj = (OBJ_HEADER *)WndCreate(ID_WINDOW1,       		// ID
		      0,0,GetMaxX(),GetMaxY(), 	// dimension
		      WND_DRAW,                	// will be dislayed after creation
		      //&mchpIcon,               	// icon
		      NULL,		              	// icon
		      NULL,	   				// set text 
		      wndScheme);

	    BtnCreate(ID_BUTTIME, 164,180,290,215,10,BTN_DRAW,           NULL,"Time",   alt3Scheme);
	    BtnCreate(ID_BUTTEMP, 164,135,290,170,10,BTN_DRAW|BTN_TOGGLE,NULL,"Temp",   alt3Scheme);
	    BtnCreate(ID_BUTSKED, 164, 90,290,125,10,BTN_DRAW|BTN_TOGGLE,NULL,"Sked",   alt3Scheme);
	    BtnCreate(ID_BUTHIST ,164, 45,290, 80,10,BTN_DRAW|BTN_TOGGLE,NULL,"History",alt3Scheme);
	    BtnCreate(ID_BUTUP1,   10,170, 70,220,10,BTN_DRAW,           NULL,"Up",     alt3Scheme);
	    BtnCreate(ID_BUTDN1,   90,170,150,220,10,BTN_DRAW,           NULL,"Dn",     alt3Scheme);

	    StCreate(ID_TEXT1,190, 10,290, 45,DRAW,                "14:21",wndScheme);
	    StCreate(ID_TEXT4, 20,100,145,150,DRAW,                "Vac",  wnd2Scheme);
	    StCreate(ID_TEXT2, 70,100,145,150,DRAW,                "Hold", wndScheme);
	    StCreate(ID_TEXT3, 20, 30,145, 60,DRAW|ST_CENTER_ALIGN,"71 F", alt4Scheme);
		formSetpoint();
	    StCreate(ID_TEXTSP, 20, 70,145,100,DRAW|ST_CENTER_ALIGN,(XCHAR *)&szSetpoint[0], wndScheme);

	    break;

	case 2:		// - - - - - - T e m p   W i n d o w - - - - - - 
	    obj = (OBJ_HEADER *)WndCreate(ID_WINDOW1,       		// ID
		      0,0,GetMaxX(),GetMaxY(), 	// dimension
		      WND_DRAW,                	// will be dislayed after creation
		      //&mchpIcon,               	// icon
		      NULL,		              	// icon
		      NULL,	   				// set text 
		      wndScheme);

	    BtnCreate(ID_BUTOK,   164,180,290,215,10,BTN_DRAW,                       NULL,"OK",      alt3Scheme);
	    BtnCreate(ID_BUTHOLD, 164,135,290,170,10,BTN_DRAW|BTN_TOGGLE,            NULL,"Hold",    alt3Scheme);
	    BtnCreate(ID_BUTNITE, 164, 90,290,125,10,BTN_DRAW|BTN_TOGGLE,            NULL,"Night",   alt3Scheme);
	    BtnCreate(ID_BUTDAY,  164, 45,290, 80,10,BTN_DRAW|BTN_PRESSED|BTN_TOGGLE,NULL,"Day",     alt3Scheme);
	    BtnCreate(ID_BUTUP2,   10,170, 70,220,10,BTN_DRAW,                       NULL,"Up",      alt3Scheme);
	    BtnCreate(ID_BUTDN2,   90,170,150,220,10,BTN_DRAW,                       NULL,"Dn",      alt3Scheme);

	    StCreate(ID_TEXT3,20,70,145,100,ST_DRAW|ST_FRAME|ST_CENTER_ALIGN,"72 F",alt4Scheme);

	    break;

	case 3:		// - - - - - - S k e d   W i n d o w - - - - - - 
	    obj = (OBJ_HEADER *)WndCreate(ID_WINDOW1,       		// ID
		      0,0,GetMaxX(),GetMaxY(), 	// dimension
		      WND_DRAW,                	// will be dislayed after creation
		      //&mchpIcon,               	// icon
		      NULL,		              	// icon
		      NULL,	   				// set text 
		      wndScheme);

	    StCreate(ID_TEXT3, 10, 10,200, 40,DRAW|ST_CENTER_ALIGN,"- - - D A Y - - -",wndScheme);
	    StCreate(ID_TEXT5, 40, 40, 75, 70,DRAW|ST_CENTER_ALIGN,"07",               alt4Scheme);
	    StCreate(ID_TEXT1,100, 40,120, 70,DRAW|ST_CENTER_ALIGN,":",                wndScheme);
	    StCreate(ID_TEXT6,130, 40,165, 70,DRAW|ST_CENTER_ALIGN,"00",               alt4Scheme);
	    BtnCreate(ID_BUTUP3,  10, 70, 50,110,10,BTN_DRAW,           NULL,"Up",      alt3Scheme);
	    BtnCreate(ID_BUTDN3,  60, 70,100,110,10,BTN_DRAW,           NULL,"Dn",      alt3Scheme);
	    BtnCreate(ID_BUTUP4, 110, 70,150,110,10,BTN_DRAW,           NULL,"Up",      alt3Scheme);
	    BtnCreate(ID_BUTDN4, 160, 70,200,110,10,BTN_DRAW,           NULL,"Dn",      alt3Scheme);

	    StCreate(ID_TEXT4, 10,120,200,150,DRAW|ST_CENTER_ALIGN,"- - N I G H T - -",wndScheme);
	    StCreate(ID_TEXT7, 40,150, 75,180,DRAW|ST_CENTER_ALIGN,"23",               alt4Scheme);
	    StCreate(ID_TEXT2,100,150,120,180,DRAW|ST_CENTER_ALIGN,":",                wndScheme);
	    StCreate(ID_TEXT8,130,150,165,180,DRAW|ST_CENTER_ALIGN,"30",               alt4Scheme);
	    BtnCreate(ID_BUTUP5,  10,180, 50,220,10,BTN_DRAW,           NULL,"Up",      alt3Scheme);
	    BtnCreate(ID_BUTDN5,  60,180,100,220,10,BTN_DRAW,           NULL,"Dn",      alt3Scheme);
	    BtnCreate(ID_BUTUP6, 110,180,150,220,10,BTN_DRAW,           NULL,"Up",      alt3Scheme);
	    BtnCreate(ID_BUTDN6, 160,180,200,220,10,BTN_DRAW,           NULL,"Dn",      alt3Scheme);

	    BtnCreate(ID_BUTOK,   220,180,300,215,10,BTN_DRAW,            NULL,"OK",      alt3Scheme);

	    break;

	case 4:		// - - - - - - H i s t   W i n d o w - - - - - -
		// Color the screen with the background color
		SetColor( wndScheme->CommonBkColor );
		Bar(0,0,319,239);

		// Make the graph background black
		SetColor( BLACK );
		Bar(GR_PAN_LEF,GR_PAN_TOP,GR_PAN_RIG,GR_PAN_BOT);

		// Generate gridlines
		for ( x=0; x<100; x+=20 )
		{
			SetColor( wndScheme->EmbossDkColor );
			yp1 = GR_PAN_BOT - x*2;
			Line( GR_PAN_LEF, yp1, GR_PAN_RIG, yp1);
			itoa( x, szWork );
			SetColor( wndScheme->TextColor0 );
			OutTextXY( GR_PAN_RIG+2, yp1-15, szWork);
		}

		// Set a border on the graph
		SetColor( wndScheme->EmbossLtColor );
		Rectangle(GR_PAN_LEF-1,GR_PAN_TOP-1,GR_PAN_RIG+1,GR_PAN_BOT+1);

		// Draw the data lines on the graph
		SetLineThickness( 1 );
		for ( x=10; x<74; x++ )
		{
			SetColor( RGB565CONVERT( 0x00, 0xe0, 0x00) );
			xp1 = GR_PAN_LEF + (x-10)*3;
			xp2 = GR_PAN_LEF + (x-9)*3;
			yp1 = GR_PAN_BOT - nOutdoor[x]*2;
			yp2 = GR_PAN_BOT - nOutdoor[x+1]*2;
			Line( xp1,yp1,xp2,yp2);
			SetColor( RGB565CONVERT( 0xff, 0x20, 0x20) );
			yp1 = GR_PAN_BOT - nIndoor[x]*2;
			yp2 = GR_PAN_BOT - nIndoor[x+1]*2;
			Line( xp1,yp1,xp2,yp2);
		}
	    BtnCreate(ID_BUTOK,   230,195,310,235,10,BTN_DRAW,           NULL,"OK",      alt3Scheme);

		break;

	default:
	    break;
	}
	nNewPage = 0;

}
