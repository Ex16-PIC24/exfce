#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
include Makefile

# Environment
MKDIR=mkdir -p
RM=rm -f 
CP=cp 
# Macros
CND_CONF=default

ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/ExFce-colors.X.${IMAGE_TYPE}.elf
else
IMAGE_TYPE=production
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/ExFce-colors.X.${IMAGE_TYPE}.elf
endif
# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}
# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1472/ExFce_main-colors.o ${OBJECTDIR}/_ext/1472/eeprom.o ${OBJECTDIR}/_ext/1472/TouchScreen.o ${OBJECTDIR}/_ext/1472/configbits.o ${OBJECTDIR}/_ext/1782925355/Pictures.o


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

# Path to java used to run MPLAB X when this makefile was created
MP_JAVA_PATH=/usr/lib/jvm/java-1.6.0-openjdk-1.6.0.0.x86_64/jre/bin/
OS_ORIGINAL="Linux"
OS_CURRENT="$(shell uname -s)"
############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
MP_CC=/opt/microchip/mplabc30/v3.24/bin/pic30-gcc
# MP_BC is not defined
MP_AS=/opt/microchip/mplabc30/v3.24/bin/pic30-as
MP_LD=/opt/microchip/mplabc30/v3.24/bin/pic30-ld
MP_AR=/opt/microchip/mplabc30/v3.24/bin/pic30-ar
# MP_BC is not defined
MP_CC_DIR=/opt/microchip/mplabc30/v3.24/bin
# MP_BC_DIR is not defined
MP_AS_DIR=/opt/microchip/mplabc30/v3.24/bin
MP_LD_DIR=/opt/microchip/mplabc30/v3.24/bin
MP_AR_DIR=/opt/microchip/mplabc30/v3.24/bin
# MP_BC_DIR is not defined
.build-conf: ${BUILD_SUBPROJECTS}
ifneq ($(OS_CURRENT),$(OS_ORIGINAL))
	@echo "***** WARNING: This make file contains OS dependent code. The OS this makefile is being run is different from the OS it was created in."
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/ExFce-colors.X.${IMAGE_TYPE}.elf

MP_PROCESSOR_OPTION=24FJ128GA010
MP_LINKER_FILE_OPTION=,--script=../p24FJ128GA010.gld
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1472/ExFce_main-colors.o: ../ExFce_main-colors.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR}/_ext/1472 
	${RM} ${OBJECTDIR}/_ext/1472/ExFce_main-colors.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD2=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I".." -I"../toolkit/Microchip/Include/Graphics" -I"../toolkit/Microchip/Include" -MMD -MF ${OBJECTDIR}/_ext/1472/ExFce_main-colors.o.d -o ${OBJECTDIR}/_ext/1472/ExFce_main-colors.o ../ExFce_main-colors.c  
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@sed -e 's/\"//g' -e 's/\\$$/__EOL__/g' -e 's/\\ /__ESCAPED_SPACES__/g' -e 's/\\/\//g' -e 's/__ESCAPED_SPACES__/\\ /g' -e 's/__EOL__$$/\\/g' ${OBJECTDIR}/_ext/1472/ExFce_main-colors.o.d > ${OBJECTDIR}/_ext/1472/ExFce_main-colors.o.tmp
	${RM} ${OBJECTDIR}/_ext/1472/ExFce_main-colors.o.d 
	${CP} ${OBJECTDIR}/_ext/1472/ExFce_main-colors.o.tmp ${OBJECTDIR}/_ext/1472/ExFce_main-colors.o.d 
	${RM} ${OBJECTDIR}/_ext/1472/ExFce_main-colors.o.tmp}
else 
	@sed -e 's/\"//g' ${OBJECTDIR}/_ext/1472/ExFce_main-colors.o.d > ${OBJECTDIR}/_ext/1472/ExFce_main-colors.o.tmp
	${RM} ${OBJECTDIR}/_ext/1472/ExFce_main-colors.o.d 
	${CP} ${OBJECTDIR}/_ext/1472/ExFce_main-colors.o.tmp ${OBJECTDIR}/_ext/1472/ExFce_main-colors.o.d 
	${RM} ${OBJECTDIR}/_ext/1472/ExFce_main-colors.o.tmp
endif
${OBJECTDIR}/_ext/1472/eeprom.o: ../eeprom.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR}/_ext/1472 
	${RM} ${OBJECTDIR}/_ext/1472/eeprom.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD2=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I".." -I"../toolkit/Microchip/Include/Graphics" -I"../toolkit/Microchip/Include" -MMD -MF ${OBJECTDIR}/_ext/1472/eeprom.o.d -o ${OBJECTDIR}/_ext/1472/eeprom.o ../eeprom.c  
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@sed -e 's/\"//g' -e 's/\\$$/__EOL__/g' -e 's/\\ /__ESCAPED_SPACES__/g' -e 's/\\/\//g' -e 's/__ESCAPED_SPACES__/\\ /g' -e 's/__EOL__$$/\\/g' ${OBJECTDIR}/_ext/1472/eeprom.o.d > ${OBJECTDIR}/_ext/1472/eeprom.o.tmp
	${RM} ${OBJECTDIR}/_ext/1472/eeprom.o.d 
	${CP} ${OBJECTDIR}/_ext/1472/eeprom.o.tmp ${OBJECTDIR}/_ext/1472/eeprom.o.d 
	${RM} ${OBJECTDIR}/_ext/1472/eeprom.o.tmp}
else 
	@sed -e 's/\"//g' ${OBJECTDIR}/_ext/1472/eeprom.o.d > ${OBJECTDIR}/_ext/1472/eeprom.o.tmp
	${RM} ${OBJECTDIR}/_ext/1472/eeprom.o.d 
	${CP} ${OBJECTDIR}/_ext/1472/eeprom.o.tmp ${OBJECTDIR}/_ext/1472/eeprom.o.d 
	${RM} ${OBJECTDIR}/_ext/1472/eeprom.o.tmp
endif
${OBJECTDIR}/_ext/1472/TouchScreen.o: ../TouchScreen.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR}/_ext/1472 
	${RM} ${OBJECTDIR}/_ext/1472/TouchScreen.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD2=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I".." -I"../toolkit/Microchip/Include/Graphics" -I"../toolkit/Microchip/Include" -MMD -MF ${OBJECTDIR}/_ext/1472/TouchScreen.o.d -o ${OBJECTDIR}/_ext/1472/TouchScreen.o ../TouchScreen.c  
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@sed -e 's/\"//g' -e 's/\\$$/__EOL__/g' -e 's/\\ /__ESCAPED_SPACES__/g' -e 's/\\/\//g' -e 's/__ESCAPED_SPACES__/\\ /g' -e 's/__EOL__$$/\\/g' ${OBJECTDIR}/_ext/1472/TouchScreen.o.d > ${OBJECTDIR}/_ext/1472/TouchScreen.o.tmp
	${RM} ${OBJECTDIR}/_ext/1472/TouchScreen.o.d 
	${CP} ${OBJECTDIR}/_ext/1472/TouchScreen.o.tmp ${OBJECTDIR}/_ext/1472/TouchScreen.o.d 
	${RM} ${OBJECTDIR}/_ext/1472/TouchScreen.o.tmp}
else 
	@sed -e 's/\"//g' ${OBJECTDIR}/_ext/1472/TouchScreen.o.d > ${OBJECTDIR}/_ext/1472/TouchScreen.o.tmp
	${RM} ${OBJECTDIR}/_ext/1472/TouchScreen.o.d 
	${CP} ${OBJECTDIR}/_ext/1472/TouchScreen.o.tmp ${OBJECTDIR}/_ext/1472/TouchScreen.o.d 
	${RM} ${OBJECTDIR}/_ext/1472/TouchScreen.o.tmp
endif
${OBJECTDIR}/_ext/1472/configbits.o: ../configbits.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR}/_ext/1472 
	${RM} ${OBJECTDIR}/_ext/1472/configbits.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD2=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I".." -I"../toolkit/Microchip/Include/Graphics" -I"../toolkit/Microchip/Include" -MMD -MF ${OBJECTDIR}/_ext/1472/configbits.o.d -o ${OBJECTDIR}/_ext/1472/configbits.o ../configbits.c  
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@sed -e 's/\"//g' -e 's/\\$$/__EOL__/g' -e 's/\\ /__ESCAPED_SPACES__/g' -e 's/\\/\//g' -e 's/__ESCAPED_SPACES__/\\ /g' -e 's/__EOL__$$/\\/g' ${OBJECTDIR}/_ext/1472/configbits.o.d > ${OBJECTDIR}/_ext/1472/configbits.o.tmp
	${RM} ${OBJECTDIR}/_ext/1472/configbits.o.d 
	${CP} ${OBJECTDIR}/_ext/1472/configbits.o.tmp ${OBJECTDIR}/_ext/1472/configbits.o.d 
	${RM} ${OBJECTDIR}/_ext/1472/configbits.o.tmp}
else 
	@sed -e 's/\"//g' ${OBJECTDIR}/_ext/1472/configbits.o.d > ${OBJECTDIR}/_ext/1472/configbits.o.tmp
	${RM} ${OBJECTDIR}/_ext/1472/configbits.o.d 
	${CP} ${OBJECTDIR}/_ext/1472/configbits.o.tmp ${OBJECTDIR}/_ext/1472/configbits.o.d 
	${RM} ${OBJECTDIR}/_ext/1472/configbits.o.tmp
endif
${OBJECTDIR}/_ext/1782925355/Pictures.o: ../bitmaps/Pictures.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR}/_ext/1782925355 
	${RM} ${OBJECTDIR}/_ext/1782925355/Pictures.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD2=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I".." -I"../toolkit/Microchip/Include/Graphics" -I"../toolkit/Microchip/Include" -MMD -MF ${OBJECTDIR}/_ext/1782925355/Pictures.o.d -o ${OBJECTDIR}/_ext/1782925355/Pictures.o ../bitmaps/Pictures.c  
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@sed -e 's/\"//g' -e 's/\\$$/__EOL__/g' -e 's/\\ /__ESCAPED_SPACES__/g' -e 's/\\/\//g' -e 's/__ESCAPED_SPACES__/\\ /g' -e 's/__EOL__$$/\\/g' ${OBJECTDIR}/_ext/1782925355/Pictures.o.d > ${OBJECTDIR}/_ext/1782925355/Pictures.o.tmp
	${RM} ${OBJECTDIR}/_ext/1782925355/Pictures.o.d 
	${CP} ${OBJECTDIR}/_ext/1782925355/Pictures.o.tmp ${OBJECTDIR}/_ext/1782925355/Pictures.o.d 
	${RM} ${OBJECTDIR}/_ext/1782925355/Pictures.o.tmp}
else 
	@sed -e 's/\"//g' ${OBJECTDIR}/_ext/1782925355/Pictures.o.d > ${OBJECTDIR}/_ext/1782925355/Pictures.o.tmp
	${RM} ${OBJECTDIR}/_ext/1782925355/Pictures.o.d 
	${CP} ${OBJECTDIR}/_ext/1782925355/Pictures.o.tmp ${OBJECTDIR}/_ext/1782925355/Pictures.o.d 
	${RM} ${OBJECTDIR}/_ext/1782925355/Pictures.o.tmp
endif
else
${OBJECTDIR}/_ext/1472/ExFce_main-colors.o: ../ExFce_main-colors.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR}/_ext/1472 
	${RM} ${OBJECTDIR}/_ext/1472/ExFce_main-colors.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I".." -I"../toolkit/Microchip/Include/Graphics" -I"../toolkit/Microchip/Include" -MMD -MF ${OBJECTDIR}/_ext/1472/ExFce_main-colors.o.d -o ${OBJECTDIR}/_ext/1472/ExFce_main-colors.o ../ExFce_main-colors.c  
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@sed -e 's/\"//g' -e 's/\\$$/__EOL__/g' -e 's/\\ /__ESCAPED_SPACES__/g' -e 's/\\/\//g' -e 's/__ESCAPED_SPACES__/\\ /g' -e 's/__EOL__$$/\\/g' ${OBJECTDIR}/_ext/1472/ExFce_main-colors.o.d > ${OBJECTDIR}/_ext/1472/ExFce_main-colors.o.tmp
	${RM} ${OBJECTDIR}/_ext/1472/ExFce_main-colors.o.d 
	${CP} ${OBJECTDIR}/_ext/1472/ExFce_main-colors.o.tmp ${OBJECTDIR}/_ext/1472/ExFce_main-colors.o.d 
	${RM} ${OBJECTDIR}/_ext/1472/ExFce_main-colors.o.tmp}
else 
	@sed -e 's/\"//g' ${OBJECTDIR}/_ext/1472/ExFce_main-colors.o.d > ${OBJECTDIR}/_ext/1472/ExFce_main-colors.o.tmp
	${RM} ${OBJECTDIR}/_ext/1472/ExFce_main-colors.o.d 
	${CP} ${OBJECTDIR}/_ext/1472/ExFce_main-colors.o.tmp ${OBJECTDIR}/_ext/1472/ExFce_main-colors.o.d 
	${RM} ${OBJECTDIR}/_ext/1472/ExFce_main-colors.o.tmp
endif
${OBJECTDIR}/_ext/1472/eeprom.o: ../eeprom.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR}/_ext/1472 
	${RM} ${OBJECTDIR}/_ext/1472/eeprom.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I".." -I"../toolkit/Microchip/Include/Graphics" -I"../toolkit/Microchip/Include" -MMD -MF ${OBJECTDIR}/_ext/1472/eeprom.o.d -o ${OBJECTDIR}/_ext/1472/eeprom.o ../eeprom.c  
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@sed -e 's/\"//g' -e 's/\\$$/__EOL__/g' -e 's/\\ /__ESCAPED_SPACES__/g' -e 's/\\/\//g' -e 's/__ESCAPED_SPACES__/\\ /g' -e 's/__EOL__$$/\\/g' ${OBJECTDIR}/_ext/1472/eeprom.o.d > ${OBJECTDIR}/_ext/1472/eeprom.o.tmp
	${RM} ${OBJECTDIR}/_ext/1472/eeprom.o.d 
	${CP} ${OBJECTDIR}/_ext/1472/eeprom.o.tmp ${OBJECTDIR}/_ext/1472/eeprom.o.d 
	${RM} ${OBJECTDIR}/_ext/1472/eeprom.o.tmp}
else 
	@sed -e 's/\"//g' ${OBJECTDIR}/_ext/1472/eeprom.o.d > ${OBJECTDIR}/_ext/1472/eeprom.o.tmp
	${RM} ${OBJECTDIR}/_ext/1472/eeprom.o.d 
	${CP} ${OBJECTDIR}/_ext/1472/eeprom.o.tmp ${OBJECTDIR}/_ext/1472/eeprom.o.d 
	${RM} ${OBJECTDIR}/_ext/1472/eeprom.o.tmp
endif
${OBJECTDIR}/_ext/1472/TouchScreen.o: ../TouchScreen.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR}/_ext/1472 
	${RM} ${OBJECTDIR}/_ext/1472/TouchScreen.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I".." -I"../toolkit/Microchip/Include/Graphics" -I"../toolkit/Microchip/Include" -MMD -MF ${OBJECTDIR}/_ext/1472/TouchScreen.o.d -o ${OBJECTDIR}/_ext/1472/TouchScreen.o ../TouchScreen.c  
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@sed -e 's/\"//g' -e 's/\\$$/__EOL__/g' -e 's/\\ /__ESCAPED_SPACES__/g' -e 's/\\/\//g' -e 's/__ESCAPED_SPACES__/\\ /g' -e 's/__EOL__$$/\\/g' ${OBJECTDIR}/_ext/1472/TouchScreen.o.d > ${OBJECTDIR}/_ext/1472/TouchScreen.o.tmp
	${RM} ${OBJECTDIR}/_ext/1472/TouchScreen.o.d 
	${CP} ${OBJECTDIR}/_ext/1472/TouchScreen.o.tmp ${OBJECTDIR}/_ext/1472/TouchScreen.o.d 
	${RM} ${OBJECTDIR}/_ext/1472/TouchScreen.o.tmp}
else 
	@sed -e 's/\"//g' ${OBJECTDIR}/_ext/1472/TouchScreen.o.d > ${OBJECTDIR}/_ext/1472/TouchScreen.o.tmp
	${RM} ${OBJECTDIR}/_ext/1472/TouchScreen.o.d 
	${CP} ${OBJECTDIR}/_ext/1472/TouchScreen.o.tmp ${OBJECTDIR}/_ext/1472/TouchScreen.o.d 
	${RM} ${OBJECTDIR}/_ext/1472/TouchScreen.o.tmp
endif
${OBJECTDIR}/_ext/1472/configbits.o: ../configbits.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR}/_ext/1472 
	${RM} ${OBJECTDIR}/_ext/1472/configbits.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I".." -I"../toolkit/Microchip/Include/Graphics" -I"../toolkit/Microchip/Include" -MMD -MF ${OBJECTDIR}/_ext/1472/configbits.o.d -o ${OBJECTDIR}/_ext/1472/configbits.o ../configbits.c  
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@sed -e 's/\"//g' -e 's/\\$$/__EOL__/g' -e 's/\\ /__ESCAPED_SPACES__/g' -e 's/\\/\//g' -e 's/__ESCAPED_SPACES__/\\ /g' -e 's/__EOL__$$/\\/g' ${OBJECTDIR}/_ext/1472/configbits.o.d > ${OBJECTDIR}/_ext/1472/configbits.o.tmp
	${RM} ${OBJECTDIR}/_ext/1472/configbits.o.d 
	${CP} ${OBJECTDIR}/_ext/1472/configbits.o.tmp ${OBJECTDIR}/_ext/1472/configbits.o.d 
	${RM} ${OBJECTDIR}/_ext/1472/configbits.o.tmp}
else 
	@sed -e 's/\"//g' ${OBJECTDIR}/_ext/1472/configbits.o.d > ${OBJECTDIR}/_ext/1472/configbits.o.tmp
	${RM} ${OBJECTDIR}/_ext/1472/configbits.o.d 
	${CP} ${OBJECTDIR}/_ext/1472/configbits.o.tmp ${OBJECTDIR}/_ext/1472/configbits.o.d 
	${RM} ${OBJECTDIR}/_ext/1472/configbits.o.tmp
endif
${OBJECTDIR}/_ext/1782925355/Pictures.o: ../bitmaps/Pictures.c  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} ${OBJECTDIR}/_ext/1782925355 
	${RM} ${OBJECTDIR}/_ext/1782925355/Pictures.o.d 
	${MP_CC} $(MP_EXTRA_CC_PRE)  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I".." -I"../toolkit/Microchip/Include/Graphics" -I"../toolkit/Microchip/Include" -MMD -MF ${OBJECTDIR}/_ext/1782925355/Pictures.o.d -o ${OBJECTDIR}/_ext/1782925355/Pictures.o ../bitmaps/Pictures.c  
ifneq (,$(findstring MINGW32,$(OS_CURRENT))) 
	@sed -e 's/\"//g' -e 's/\\$$/__EOL__/g' -e 's/\\ /__ESCAPED_SPACES__/g' -e 's/\\/\//g' -e 's/__ESCAPED_SPACES__/\\ /g' -e 's/__EOL__$$/\\/g' ${OBJECTDIR}/_ext/1782925355/Pictures.o.d > ${OBJECTDIR}/_ext/1782925355/Pictures.o.tmp
	${RM} ${OBJECTDIR}/_ext/1782925355/Pictures.o.d 
	${CP} ${OBJECTDIR}/_ext/1782925355/Pictures.o.tmp ${OBJECTDIR}/_ext/1782925355/Pictures.o.d 
	${RM} ${OBJECTDIR}/_ext/1782925355/Pictures.o.tmp}
else 
	@sed -e 's/\"//g' ${OBJECTDIR}/_ext/1782925355/Pictures.o.d > ${OBJECTDIR}/_ext/1782925355/Pictures.o.tmp
	${RM} ${OBJECTDIR}/_ext/1782925355/Pictures.o.d 
	${CP} ${OBJECTDIR}/_ext/1782925355/Pictures.o.tmp ${OBJECTDIR}/_ext/1782925355/Pictures.o.d 
	${RM} ${OBJECTDIR}/_ext/1782925355/Pictures.o.tmp
endif
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/ExFce-colors.X.${IMAGE_TYPE}.elf: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -omf=elf  -mcpu=$(MP_PROCESSOR_OPTION)  -D__DEBUG -D__MPLAB_DEBUGGER_ICD2=1 -o dist/${CND_CONF}/${IMAGE_TYPE}/ExFce-colors.X.${IMAGE_TYPE}.elf ${OBJECTFILES}   ../gol.a     -Wl,--defsym=__MPLAB_BUILD=1,--heap=1024,-L"..",-Map="$(BINDIR_)$(TARGETBASE).map",--report-mem,--cref,--report-mem$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__ICD2RAM=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_ICD2=1
else
dist/${CND_CONF}/${IMAGE_TYPE}/ExFce-colors.X.${IMAGE_TYPE}.elf: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -omf=elf  -mcpu=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/ExFce-colors.X.${IMAGE_TYPE}.elf ${OBJECTFILES}   ../gol.a     -Wl,--defsym=__MPLAB_BUILD=1,--heap=1024,-L"..",-Map="$(BINDIR_)$(TARGETBASE).map",--report-mem,--cref,--report-mem$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION)
	${MP_CC_DIR}/pic30-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/ExFce-colors.X.${IMAGE_TYPE}.elf -omf=elf
endif


# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf:
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
