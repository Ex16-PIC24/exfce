#ifndef _TG1_H
#define _TG1_H

// Oscillator frequency
#define SYSCLK 32000000 // 8MHz x 4PLL

////////////////////////////// INCLUDES //////////////////////////////
#include <p24Fxxxx.h>
#include "GenericTypeDefs.h"
#include "Graphics.h"
#include "EEPROM.h"
#include "TouchScreen.h"
#include <GOL.h>
//#include "Beep.h"
//#include "SideButtons.h"
//#include "rtcc.h"

#endif
